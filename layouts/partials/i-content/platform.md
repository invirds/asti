* Primary:
  1. Input
     * type:
       1. dskp: text
       2. smp: text
       3. tab: text
       4. else: text
     * Notes: N/A

  2. Description
  
     For an application software, please choose between one or more of desktop(dskp), smartphone(smp) or tablet(tab) whichever it is available on. Please choose between `input-options` available for each. There are icons associated with each `input-option` provided, which are displayed on `softpage`. In case a text other than `input-options` forms `input-value`, such text is displayed.
     
     For application software available for desktop, there are provided checkboxes to select whether it is avaliable for x32 bit, x64 bit or both as the case may be.
     
     In case the software isn't an appplication software or there is a need to indicate that software requires a computer language, framework or certain operating environment to be used properly, `else` shall be chosen. Also, note that even in case `else` is selected, those of dskp, smp, tab shall be selected with proper `input-options`, whichever the software is available on. See [TensorFlow](/software/tensorflow/) for example.
     
     Another example, jQuery is basically a JavaScript library. So for jQuery to be used a program must be available which can run JavaScript code. Hence, in this case JavaScript might be included under `else` for jQuery. Also, as JavaScript is available on all major platforms (via browsers), hence 4 entries in `dskp` column (Linux, macOS, Windows, Unix(like)), 2 in `smp` (Android, iOS), 2 in `tab` (Android, iOS) and 1 in `else` (JavaScript) will be there.
     
     Sometimes a framework or library provides API for multiple computer languages. In such case, all such languages for which such software provides API, shall form `input-value` for `else`.

* Source:
  1. Input
     * type:
       1. dskp: text
       2. smptb: text
       3. else: text
     * Notes: any number of words supported, `comma` separates `input-value` into distinct entities forming an array of strings

  2. Description
  
     `source` relates to *where the information regarding availabilty of platforms was found*. `dskp` (in `source`) includes sources from where information was found regarding availibity of software for various platforms listed under `dskp` category. Similarly, `smptb` (in `source`) relates to `smp` and `tab` while `else` (in `source`) relates to `else`.

  3. Example:
     1. src1, src2 => 2 members of one array
