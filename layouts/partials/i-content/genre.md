* Primary:
  1. Input
     * type: text
     * Notes: any number of words supported, `comma` separates `input-value` into distinct entities forming an array of strings

  2. Description
  
     `developer` takes concerned software's developer(s) as its value. In case a developer is the *Original Developer* of the software, add `<OD>` at the end of his/her name. In case developer is *Benevolent Dictator For Life*,  add `<BDFL>` at the end of his/her name. Also, keep in mind that every comma separated entity is a distinct member of array, so ensure that you capitalize first letter after every comma. As there are many developers involved in development of softwares, including names of developers with significant contributions is suggested.

  3. Example
     1. dev1<OD>, dev2 => 2 members of one array
     2. dev1 I dev2 => one member
     3. See [darktable](/software/darktable/) and its [source file](https://gitlab.com/Softorage/softorage.gitlab.io/blob/master/content/software/darktable.md)

* Source:
  1. Input
     * type: text
     * Notes: any number of words supported, `comma` separates `input-value` into distinct entities forming an array of strings

  2. Description
  
     License document of software that contains name of developer is accepted. Including `source` (in distinctly given area) for the `input-value` is highly appreciated, but where no source can be found and in case it is a trivial matter, for example, when the very name of the software contains developer's name, then giving link to software's Wikipedia page only is accepted.

  3. Example:
     1. src1, src2 => 2 members of one array
