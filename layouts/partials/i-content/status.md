* Primary:
  1. Input
     * type: select
     * Notes: none

  2. Description
  
     Select from Active, Discontinued or Unknown considering the development and user activity of concerned software.


* Source:
  1. Input
     * type: text
     * Notes: any number of words supported, `comma` separates `input-value` into distinct entities forming an array of strings

  2. Description
  
     Accepted sources are: blog of softwares, news page, or contributions page of software(eg, `https://github.com/****/****/graphs/contributors`, `https://gitlab.com/****/****/graphs/master`) or any such source that indicates the activity of the software/project with dates.

  3. Example:
     1. src1, src2 => 2 members of one array