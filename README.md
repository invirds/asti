# SAW - Software Addition Webpage

https://softorage.gitlab.io/saw/
  
SAW gives you a GUI to input values for a software to be added to Softorage, and outputs them into a YAML file with .md format.  
This file can be push to main repo in `content/software` directory via a pull request.  
  
_The project is in beta phase (Work-In-Progress)_

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. It is wholly based on web technologies like HTML, CSS and Javascript.

### Prerequisites

A web browser, preferably Google Chrome (or Chromium Open Source Browser), or Mozilla Firefox.

### Usage

* Download a copy of project from [`Tags`](https://gitlab.com/Softorage/saw/tags) page.
* Extract it on your local machine.
* Open index.html in your favourite browser and you are good to go!

<!-- A step by step series of examples that tell you how to get a development env running

Say what the step will be

```
Give the example
```

And repeat

```
until finished
```

End with an example of getting some data out of the system or using it for a little demo -->

## Built for Softorage

## Built with

* [__Bootstrap__](https://getbootstrap.com/)
* [__Popper.js__](https://popper.js.org/)
* [__JQuery__](https://jquery.com/)
* [__Hugo__](https://gohugo.io/)
* [__Font Awesome__](https://fontawesome.com/)
<!-- * UnCSS or CriticalCSS : yet to be implemented -->
<!-- * loadCSS : yet to be implemented -->

## Contributing

_Anyone contributing benefits equally from others' contributions_  

Please see [`issues board`](https://gitlab.com/Softorage/saw/boards) or [`issues list`](https://gitlab.com/Softorage/saw/issues) to see where you can start.  
<!-- Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us. -->

## License

```
   Copyright 2018 Softorage

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
```

Project's license can be viewed at [Apache license 2.0](LICENSE).
For a quick view, see [Apache license 2.0](https://tldrlegal.com/license/apache-license-2.0-(apache-2.0)) _from tl;drLegal_.

## Acknowledgments

* [Gitlab hosting](https://about.gitlab.com/product/pages/) - Awesome free hosting for open-source projects
* [Wikimedia Commons](https://commons.wikimedia.org/wiki/Main_Page) - A hundred thanks for [allowing inline linking](https://commons.wikimedia.org/wiki/Commons:Reusing_content_outside_Wikimedia/technical#Hotlinking)
* [CSS Gradient](https://cssgradient.io/) - Fabulous gradients in CSS and easy tutorials
* [GTmetrix](https://gtmetrix.com/) - For one stop page speed recommendations
<!-- * Algolia - Super smart searches -->
<!-- * Zoho Mail - Custom email hosting -->
